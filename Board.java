import java.util.Scanner;

public class Board{
	static String[][] board = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
	static int row,col;
	static Player px = new Player("X");
	static Player po = new Player("O");
	static boolean win = true;
	
	public static void showBoard() {
		System.out.println("+---+---+---+");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(board[i][j]);
				System.out.print(" ");
			}
			System.out.print("| ");
			System.out.println();
			System.out.println("+---+---+---+");
		}
	}
	public static void inputNumber() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Please input Row and Column : ");
		row = kb.nextInt()-1;
		col = kb.nextInt()-1;
		System.out.println();
	}
	public static boolean checkInput(Player p) {
		if (row > 2 || row < 0) {
			System.out.println("====Number of Row error!! Please input only 1, 2 or 3====\n");
			return false;
		} else if (col > 2 || col < 0) {
			System.out.println("====Number of Column error!! Please input only 1, 2 or 3====\n");
			return false;
		} else if (board[row][col] == "O" || board[row][col] == "X") {
			System.out.println("====This place is already taken!! Please try again ====\n");
			return false;
		} else {
			board[row][col] = p.getName();
			return true;
		}
		
	}
	public static void newBoard() {
		board = new String[3][3];
		for(int i=0;i<3;i++) {
			for(int j =0;j<3;j++) {
				board[i][j] = "-";
			}
		}
		win = true;
	}
	public static void showScore() {
		System.out.println("=====  Score  ===== ");
		System.out.println("O's score = Win : "+po.getWinCount());
		System.out.println("X's score = Win : "+px.getWinCount());
		System.out.println("\nThank you for playing \"XO Game\"");
	}
	public static boolean checkWin(int round) {
		checkXwin();
		checkOwin();
		checkDraw(round);
		return win;
	}
	public static void checkXwin(){
		checkXwinHorizon();
		checkXwinvertical();
		checkXwindiagonal();

	}
	public static void checkXwinHorizon() {
		if(board[0][0]=="X" && board[0][1]=="X" && board[0][2]=="X") {
			showWinnerX();
		}
		else if (board[1][0]=="X" && board[1][1]=="X" && board[1][2]=="X"){
			showWinnerX();
		}
		else if (board[2][0]=="X" && board[2][1]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
		
	}
	public static void checkXwinvertical() {
		if(board[0][0]=="X" && board[1][0]=="X" && board[2][0]=="X"){
			showWinnerX();
		}
		else if(board[0][1]=="X" && board[1][1]=="X" && board[2][1]=="X"){
			showWinnerX();
		}
		else if(board[0][2]=="X" && board[1][2]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
	}
	public static void checkXwindiagonal() {
		if(board[0][0]=="X" && board[1][1]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
		else if(board[0][2]=="X" && board[1][1]=="X" && board[2][0]=="X"){
			showWinnerX();
		}
	}
	public static void showWinnerX() {
		px.addWincound();;
		win = false;
		System.out.println("\n===== X is the winner!! =====\n");
		showBoard();
	}
	public static void checkOwin() {			
		checkOwinHorizon();
		checkOwinvertical();
		checkOwindiagonal();
	}
	public static void checkOwinHorizon() {
		if(board[0][0]=="O" && board[0][1]=="O" && board[0][2]=="O")
			showWinnerO();
		else if (board[1][0]=="O" && board[1][1]=="O" && board[1][2]=="O")
			showWinnerO();
		else if (board[2][0]=="O" && board[2][1]=="O" && board[2][2]=="O")
			showWinnerO();
	}
	public static void checkOwinvertical() {
		if(board[0][0]=="O" && board[1][0]=="O" && board[2][0]=="O")
			showWinnerO();
		else if(board[0][1]=="O" && board[1][1]=="O" && board[2][1]=="O")
			showWinnerO();
		else if(board[0][2]=="O" && board[1][2]=="O" && board[2][2]=="O")
			showWinnerO();
	}
	public static void checkOwindiagonal() {
		if(board[0][0]=="O" && board[1][1]=="O" && board[2][2]=="O")
			showWinnerO();
		else if(board[0][2]=="O" && board[1][1]=="O" && board[2][0]=="O")
			showWinnerO();
	}

	public static void showWinnerO() {
		po.addWincound();
		win = false;
		System.out.println("\n===== O is the winner!! =====\n");
		showBoard();
	}
	public static void checkDraw(int round) {
		checkOdraw(round);
		checkXdraw(round);
	}
	public static void checkOdraw(int round) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == "O" && board[r][c+1] == "O" && board[r][c+2] == "O") ||
		  (board[r][c] == "O" && board[r+1][c] == "O" && board[r+2][c] == "O") || 
		  (board[r][c] == "O" && board[r+1][c+1] == "O" && board[r+2][c + 2] == "O") || 
		  (board[r+1][c] == "O" && board[r+1][c+1] == "O" && board[r+1][c+2] == "O") || 
		  (board[r+2][c] == "O" && board[r+2][c+1] == "O" && board[r+2][c+2] == "O") || 
		  (board[r][c+1] == "O" && board[r+1][c+1] == "O" && board[r+2][c+1] == "O") || 
		  (board[r][c+2] == "O" && board[r+1][c+2] == "O" && board[r+2][c+2] == "O") || 
		  (board[r][c+2] == "O" && board[r+1][c+1] == "O"&& board[r+2][c] == "O"))) {
		}
	}
	public static void checkXdraw(int round) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == "X" && board[r][c+1] == "X" && board[r][c+2] == "X")|| 
			(board[r][c] == "X" && board[r+1][c] == "X" && board[r+2][c] == "X") || 
			(board[r][c] == "X" && board[r+1][c+1] == "X" && board[r+2][c + 2] == "X") || 
			(board[r+1][c] == "X" && board[r+1][c+1] == "X" && board[r+1][c+2] == "X") || 
			(board[r+2][c] == "X" && board[r+2][c+1] == "X" && board[r+2][c+2] == "X") || 
			(board[r][c+1] == "X" && board[r+1][c+1] == "X" && board[r+2][c+1] == "X") || 
			(board[r][c+2] == "X" && board[r+1][c+2] == "X" && board[r+2][c+2] == "X") || 
			(board[r][c+2] == "X" && board[r+1][c+1] == "X"&& board[r+2][c] == "X"))) {
			showDraw();
		}
	}
	private static void showDraw() {
		win = false;
		System.out.println("\n===== Draw =====\n");
		showBoard();		
	}

}
