import java.util.Scanner;

public class XO_OOP {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		Player player = new Player("X");
		String again="Y";
		Board b = new Board();
		while(again.equals("Y")) {
			int round = 0;
			b.newBoard();
			showWelcome();
			while(round<9) {
				Board.showBoard();
				player.showTurn();
				Board.inputNumber();
				if(Board.checkInput(player)==true) {
					if(player.getName()=="X")
						player.changeName("O");
					else
						player.changeName("X");
					round++;
				}
				if(Board.checkWin(round)==false) {
					break;
				}
			}
			showAgain();
			again = kb.next();
		}
		Board.showScore();
	}

	public static void showWelcome() {
		System.out.println("=====  Welcome to XO GAME  ===== ");
	}
	public static void showAgain() {
		System.out.println("Do you want to play again? (N/Y)");
	}
	

}


